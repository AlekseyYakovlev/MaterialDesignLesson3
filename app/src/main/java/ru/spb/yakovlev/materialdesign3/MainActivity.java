package ru.spb.yakovlev.materialdesign3;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initFAB();

        initMainButtonAndCheckBox();
    }

    private void initMainButtonAndCheckBox() {
        Button mainButton = findViewById(R.id.bt_main_button);
        CheckBox cbButtonEnabler = findViewById(R.id.cb_button_enabler);

        cbButtonEnabler.setOnCheckedChangeListener((buttonView, isChecked) ->
                mainButton.setEnabled(isChecked));

        mainButton.setOnClickListener(b -> new AlertDialog.Builder(b.getContext())
                .setTitle("Dialog")
                .setMessage("Press \"Ok\" button")
                .setPositiveButton("Ok", (dialog, which) ->
                        Snackbar.make(b, "Some text to show", Snackbar.LENGTH_LONG)
                                .show())
                .create()
                .show());
    }

    private void initFAB() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view ->
                Snackbar.make(view, "Photo added.", Snackbar.LENGTH_LONG).show());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
